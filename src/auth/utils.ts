import { get } from 'lodash';
import {NullableString} from "../core/Types/common";
import config from "../config";
import { tokenStorage } from "./tokenStorage";

export const buildHeaderToken = (token: NullableString): string => {
    return token ? `${config.api.token.valuePrefix}${token}` : '';
}

export const buildApiHeaders = (): Record<string, string> => {
    const token = tokenStorage.getToken();
    return {
        [config.api.token.headerName]: token ? `${config.api.token.valuePrefix}${token}` : ''
    }
}

export const formatApiUrl = (resource, id = null): string => {
    return [config.api.entrypoint.root, resource, id].join('/');
}

export const extractErrorMessage = (error) => {
    let message = get(error, 'response.data.message');

    if (message) {
        return message;
    }

    return error;
}
