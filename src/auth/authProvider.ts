import { tokenStorage } from './tokenStorage';
import config from "../config";
import jwt_decode from 'jwt-decode';
import { userStorage } from "../core/Model/SessionUser/userStorage";
import { extractErrorMessage } from "./utils";

const axios = require('axios');

const rejectError = (error) => {
    if (config.debug) {
        console.log(error);
    }
    return Promise.reject(extractErrorMessage(error));
}

export const authProvider = {
    login: (data) =>  {
        const { username: email, password } = data;
        return axios.post(
            config.api.entrypoint.adminLogin, {
                email,
                password
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then(({ data }) => {
            const token = data[config.api.token.serverTokenKey];
            const decodedToken = jwt_decode(token);
            decodedToken.name = email

            tokenStorage.setToken(token);
            userStorage.setUser(decodedToken);
            //force reload
            window.location.reload();
        }).catch((error) => {
            return rejectError(error);
        });
    },
    logout: () => {
        return axios.post(config.api.entrypoint.adminLogout)
            .then((response) => {
                tokenStorage.removeToken();
                userStorage.removeUser();
            }).catch((error) => {
                tokenStorage.removeToken();
                userStorage.removeUser();
                window.location.reload();
            });
    },
    checkAuth: params => {
        return tokenStorage.hasToken()
            ? Promise.resolve()
            : Promise.reject();
    },
    checkError: ({ response, error }) => {
        let errorStatus;

        if (error && !!error.status) {
            errorStatus = error.status;
        }

        if (response && !!response.status) {
            errorStatus = response.status;
        }

        switch (errorStatus) {
            case 401:
                tokenStorage.removeToken();
                userStorage.removeUser();
                return Promise.reject();
            default:
                return Promise.resolve();
        }
    },
    getPermissions: params => {
        return Promise.resolve();
    },
};
