import config from "../config";

const localTokenKey = config.api.token.localTokenKey;

export const tokenStorage = {
    setToken: (token: string) => {
        localStorage.setItem(localTokenKey, token);
    },
    getToken: (): string|null => {
        return localStorage.getItem(localTokenKey);
    },
    hasToken: (): boolean => {
        return !!localStorage.getItem(localTokenKey);
    },
    removeToken: (): void => {
        localStorage.removeItem(localTokenKey);
    }
}