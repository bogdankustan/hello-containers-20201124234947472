import React from "react";
import { FunctionField } from 'react-admin';

const BeneficiaryNameAndPhoneField = (props) => (
    <FunctionField {...props} render={record => `${record.beneficiaryName} (${record.beneficiaryPhone})`}/>
)

BeneficiaryNameAndPhoneField.defaultProps = { label: "Beneficiary" }

export default BeneficiaryNameAndPhoneField;