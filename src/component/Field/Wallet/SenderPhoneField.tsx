import React from "react";
import PhoneField from "../User/PhoneField";

const SenderPhoneField = (props) => (
    <PhoneField {...props}/>
)

SenderPhoneField.defaultProps = { label: "Sender phone", source: "senderPhone" }

export default SenderPhoneField;
