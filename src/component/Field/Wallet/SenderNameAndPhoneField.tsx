import React from "react";
import { FunctionField } from 'react-admin';

const SenderNameAndPhoneField = (props) => (
    <FunctionField {...props} render={record => `${record.senderName} (${record.senderPhone})`}/>
)

SenderNameAndPhoneField.defaultProps = { label: "Sender" }

export default SenderNameAndPhoneField;