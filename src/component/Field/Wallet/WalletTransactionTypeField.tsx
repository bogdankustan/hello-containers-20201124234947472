import React from "react";
import {
    WALLET_TRANSACTION_TYPE_DEPOSIT,
    WALLET_TRANSACTION_TYPE_GOAL,
    WALLET_TRANSACTION_TYPE_INTEREST,
    WALLET_TRANSACTION_TYPE_INTERNAL,
    WALLET_TRANSACTION_TYPE_POCKET_MONEY,
    WALLET_TRANSACTION_TYPE_REQUEST,
} from "../../../core/Types/wallet";
import EnumTypeField from "../Common/EnumTypeField";

class WalletTransactionTypeField extends EnumTypeField
{
    static defaultProps = {
        label: 'Type',
        source: 'type',
        component: 'text'
    }

    getEnumMap(): object {
        return {
            [WALLET_TRANSACTION_TYPE_INTERNAL]: 'Internal',
            [WALLET_TRANSACTION_TYPE_INTEREST]: 'Interest',
            [WALLET_TRANSACTION_TYPE_REQUEST]: 'Request',
            [WALLET_TRANSACTION_TYPE_POCKET_MONEY]: 'Pocket-money',
            [WALLET_TRANSACTION_TYPE_GOAL]: 'Savings-goal',
            [WALLET_TRANSACTION_TYPE_DEPOSIT]: 'Investment-deposit',
        };
    }
}

export default WalletTransactionTypeField;
