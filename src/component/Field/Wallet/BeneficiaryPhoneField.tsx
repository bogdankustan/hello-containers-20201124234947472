import React from "react";
import PhoneField from "../User/PhoneField";

const BeneficiaryPhoneField = (props) => (
    <PhoneField {...props}/>
)

BeneficiaryPhoneField.defaultProps = { label: "Beneficiary phone", source: "beneficiaryPhone" }

export default BeneficiaryPhoneField;
