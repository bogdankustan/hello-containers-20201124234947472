import React from "react";
import { FieldProps } from "../../../core/Types/common";
import BooleanChip from "../../Element/Chip/BooleanChip";

type sizeType = 'small' | 'medium';
type colorType = 'default' | 'primary' | 'secondary';

interface Props {
    source: string,
    labelTrue?: string
    labelFalse?: string
    iconTrue?: React.ReactElement | null
    iconFalse?: React.ReactElement | null
    colorTrue?: colorType,
    colorFalse?: colorType,
    size?: sizeType
}

const BooleanField = (props: FieldProps & Props) => {
    const { source, record } = props;
    return <BooleanChip value={!!record[source]} {...props} />
}

export default BooleanField;
