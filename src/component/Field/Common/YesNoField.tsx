import React from "react";
import { FieldProps } from "../../../core/Types/common";
import BooleanChip from "../../Element/Chip/BooleanChip";

const YesNoField = (props: FieldProps) => {
    const { record, source, reverse } = props;
    return <BooleanChip value={reverse ? !record[source] : !!record[source]} labelTrue="Yes" labelFalse="No"/>;
}

export default YesNoField;