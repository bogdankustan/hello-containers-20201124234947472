import { Typography } from "@material-ui/core";
import React from "react";
import moment from 'moment';
import { FieldProps } from "../../../core/Types/common";

interface Props extends FieldProps {
    emptyValue?: any
}

const UserAgeField = (props: Props ) => {
    const { record: { birthDate }, emptyValue } = props;

    if (!birthDate) {
        return <Typography>{emptyValue}</Typography>;
    }

    const dateFormatted = moment(birthDate).format('YYYY-MM-DD');
    const age = moment().diff(birthDate, 'years');

    return <Typography>{age} years <small>({dateFormatted})</small></Typography>;
}

UserAgeField.defaultProps = { source: 'birthDate', label: 'Age', emptyValue: '(no value)'}

export default UserAgeField;