import React from "react";
import BooleanField from "../Common/BooleanField";

const UserInviteStatusField = (props) => {
    return <BooleanField {...props} labelTrue="Accepted" labelFalse="Pending"/>;
}

UserInviteStatusField.defaultProps = { label: "Status", source: 'isAccepted' }

export default UserInviteStatusField;
