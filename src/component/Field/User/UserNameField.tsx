import React from "react";
import { TextField } from 'react-admin';

const UserNameField = (props) => (
    <TextField {...props}/>
)

UserNameField.defaultProps = { label: "Name", source: "name" }

export default UserNameField;
