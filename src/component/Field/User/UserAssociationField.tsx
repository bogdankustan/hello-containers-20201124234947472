import React from "react";
import { Error, LinearProgress } from 'react-admin';
import BooleanChip from "../../Element/Chip/BooleanChip";
import { useFetchRelatedUserInvites } from "../../../core/Hooks/Api";

const AssociationChips = (props) => {
    const { record } = props;
    const { loading, error, data } = useFetchRelatedUserInvites(record);

    if (loading) return <LinearProgress />;
    if (error) return <Error />;
    if (!data) return null;

    const invites = data.map(item => (
        item.sender === record.id
            ? { name: item.receiverName, phone: item.receiverPhone, accepted: item.isAccepted }
            : { name: item.senderName, phone: item.senderPhone, accepted: item.isAccepted }
        )
    );

    if (invites.length === 0) {
        return <BooleanChip value={false} labelFalse="None" />
    }

    return (
        <>
        {invites.map((item, key) => (
            <BooleanChip value={item.accepted} labelFalse={item.name} labelTrue={item.name} key={key}/>
        ))}
        </>
    );
}

const UserAssociationField = (props) => (
    <AssociationChips {...props}/>
);

UserAssociationField.defaultProps = { label: 'Association(s)', sortable: false}

export default UserAssociationField;
