import React from "react";
import { Chip } from '@material-ui/core';
import { Phone } from '@material-ui/icons';

export const PhoneChip = ({ phone }) => {
    return <Chip icon={<Phone/>} label={phone} size="small"/>
}
