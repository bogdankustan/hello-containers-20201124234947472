import React, {Fragment} from "react";
import {BulkDeleteButton, BulkExportButton, List} from 'react-admin';

const PostBulkActionButtons = props => (
    <Fragment>
        <BulkExportButton {...props} />
        <BulkDeleteButton {...props} undoable={false}/>
    </Fragment>
);

export default (props) => (
    <List {...props} bulkActionButtons={<PostBulkActionButtons />}>
    </List>
);