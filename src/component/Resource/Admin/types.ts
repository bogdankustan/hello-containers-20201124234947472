import { ROLE_ADMIN, ROLE_SUPER_ADMIN } from "../../../core/Types/roles";

export const RoleChoices = [
    { id: ROLE_ADMIN,       name: "Admin" },
    { id: ROLE_SUPER_ADMIN, name: "Super admin" },
];
