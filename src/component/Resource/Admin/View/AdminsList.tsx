import React from "react";
import ResourceIdField from "../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../Field/Common/CreatedAtField";
import {
    Datagrid,
    TextField,
    ShowButton,
    EditButton,
} from 'react-admin';
import UpdatedAtField from "../../../Field/Common/UpdatedAtField";
import LastLoginField from "../../../Field/Common/LastLoginField";
import useStyles from "../../../../styles";
import ListConfirmDelete from "../../../Element/List/ListConfirmDelete";

export const AdminsList = (props) => {
    const classes = useStyles();
    return (
        <ListConfirmDelete {...props}>
            <Datagrid>
                <ResourceIdField cellClassName={classes.narrow}/>
                <CreatedAtField cellClassName={classes.narrow}/>
                <UpdatedAtField cellClassName={classes.narrow}/>
                <TextField source="name" cellClassName={classes.narrow}/>
                <TextField source="email"/>
                <LastLoginField cellClassName={classes.narrow}/>
                <ShowButton cellClassName={classes.narrow}/>
                <EditButton cellClassName={classes.narrow}/>
            </Datagrid>
        </ListConfirmDelete>
    );
};
