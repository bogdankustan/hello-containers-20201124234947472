import { USER_GROUP_ALL, USER_GROUP_CHILD, USER_GROUP_PARENT } from "../../../core/Types/notification";

export const UserGroupChoices = [
    { id: USER_GROUP_ALL,    name: "All users" },
    { id: USER_GROUP_PARENT, name: "Parent users" },
    { id: USER_GROUP_CHILD,  name: "Child users" },
];