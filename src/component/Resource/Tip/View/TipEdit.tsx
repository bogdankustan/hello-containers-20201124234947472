import React from "react";
import {
    BooleanInput,
    Edit,
    ImageField,
    ImageInput,
    SimpleForm,
    TextInput,
    required
} from 'react-admin';
import RichTextInput from "ra-input-rich-text";

export const TipEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="title" validate={required()} />
            <RichTextInput multiline source="description" validate={required()} />
            <RichTextInput multiline source="text" validate={required()} />
            <ImageField source="image" title="Image" />
            <ImageInput source="encodedImage" label="Change image" accept="image/*">
                <ImageField source="image" title="Image" />
            </ImageInput>
            <BooleanInput source="removeImage" />
        </SimpleForm>
    </Edit>
);