import React from "react";
import {
    Create,
    SelectInput,
    SimpleForm,
    TextInput,
    required
} from 'react-admin';
import RichTextInput from "ra-input-rich-text";
import {PageCategoryChoices} from "../categories";
import {CATEGORY_OTHER} from "../../../../core/Types/page";

export const PageCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput multiline source="title" validate={required()} />
            <SelectInput
                source="category"
                choices={PageCategoryChoices}
                validate={required()}
                defaultValue={CATEGORY_OTHER}
            />
            <RichTextInput multiline source="description" validate={required()} />
        </SimpleForm>
    </Create>
);