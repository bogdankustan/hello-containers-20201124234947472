import React from "react";
import {
    Datagrid,
    EditButton,
    ReferenceField,
    RichTextField,
    ShowButton,
    TextField,
} from 'react-admin';
import ResourceIdField from "../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../Field/Common/CreatedAtField";
import UpdatedAtField from "../../../Field/Common/UpdatedAtField";
import ListConfirmDelete from "../../../Element/List/ListConfirmDelete";

export const LessonMaterialsList = (props) => (
    <ListConfirmDelete {...props}>
        <Datagrid>
            <ResourceIdField record />
            <CreatedAtField record />
            <UpdatedAtField record />
            <RichTextField source="title"/>
            <ReferenceField label="Lesson" source="lesson" reference="lessons">
                <TextField source="title" />
            </ReferenceField>
            <TextField source="position"/>
            <ShowButton />
            <EditButton />
        </Datagrid>
    </ListConfirmDelete>
);