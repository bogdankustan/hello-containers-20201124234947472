import React from "react";
import {Create, SimpleForm} from 'react-admin';

export const TaskCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
        </SimpleForm>
    </Create>
);