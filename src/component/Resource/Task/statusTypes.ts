export const StatusChoices = [
    { id: -10, name: 'Rejected' },
    { id: 0, name: 'Approving' },
    { id: 10, name: 'In progress' },
    { id: 20, name: 'Evaluating' },
    { id: 30, name: 'Completed' },
];