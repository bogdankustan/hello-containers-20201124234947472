export default [
    { id: 'Card manufacture', name: 'Card manufacture' },
    { id: 'Card shipping', name: 'Card shipping' },
    { id: 'Transaction fee for Premium user', name: 'Transaction fee for Premium user' },
    { id: 'Transaction fee for free user', name: 'Transaction fee for free user' },
];