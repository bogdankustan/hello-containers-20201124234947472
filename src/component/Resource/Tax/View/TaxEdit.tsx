import React from "react";
import {
    Edit,
    NumberInput,
    SelectInput,
    SimpleForm,
    required
} from 'react-admin';
import {EuropeCountryChoices} from "../../../../core/Types/country";
import TaxCategories from '../categories'

export default (props) => (
    <Edit {...props}>
        <SimpleForm>
            <SelectInput source="category" validate={required()} choices={TaxCategories} />
            <SelectInput source="country" validate={required()} choices={EuropeCountryChoices} />
            <NumberInput source="price" step={0.01} min={0} validate={required()}/>
        </SimpleForm>
    </Edit>
);