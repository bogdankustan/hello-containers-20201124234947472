import React from "react";
import {
    Datagrid,
    EditButton,
    ReferenceField,
    ShowButton,
    NumberField,
    TextField,
} from 'react-admin';
import ResourceIdField from "../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../Field/Common/CreatedAtField";
import UpdatedAtField from "../../../Field/Common/UpdatedAtField";
import ListConfirmDelete from "../../../Element/List/ListConfirmDelete";

export default (props) => (
    <ListConfirmDelete {...props}>
        <Datagrid>
            <ResourceIdField record />
            <CreatedAtField record />
            <UpdatedAtField record />
            <TextField source="title" />
            <ReferenceField
                label="Subscription duration"
                source="subscriptionDuration"
                reference="subscription-durations"
                sortBy="subscriptionDuration.months">
                <NumberField source="months" />
            </ReferenceField>
            <NumberField source="price" />
            <ShowButton />
            <EditButton />
        </Datagrid>
    </ListConfirmDelete>
);