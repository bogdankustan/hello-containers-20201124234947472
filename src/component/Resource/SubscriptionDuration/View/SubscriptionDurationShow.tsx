import * as React from "react";
import {
    NumberField,
    Show,
    SimpleShowLayout
} from 'react-admin';
import CreatedAtField from "../../../Field/Common/CreatedAtField";
import UpdatedAtField from "../../../Field/Common/UpdatedAtField";

export default (props) => (
    <Show {...props}>
        <SimpleShowLayout>
            <NumberField source="months" />
            <CreatedAtField addLabel />
            <UpdatedAtField addLabel />
        </SimpleShowLayout>
    </Show>
);
