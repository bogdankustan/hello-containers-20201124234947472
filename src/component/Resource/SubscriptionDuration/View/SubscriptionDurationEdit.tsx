import React from "react";
import {
    Edit,
    NumberInput,
    SimpleForm,
    required
} from 'react-admin';

export default (props) => {
    return (
        <Edit {...props}>
            <SimpleForm>
                <NumberInput source="months" step={1} min={1} validate={required()} />
            </SimpleForm>
        </Edit>
    );
}
