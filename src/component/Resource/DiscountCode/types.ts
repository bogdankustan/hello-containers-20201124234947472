import {PERCENT, FIXED} from "../../../core/Types/DiscountCodeTypes";

export default [
    {id: FIXED, name: 'Fixed'},
    {id: PERCENT, name: 'Percent'},
];