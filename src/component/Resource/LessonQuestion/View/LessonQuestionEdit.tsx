import React from "react";
import {Edit, NumberInput, ReferenceInput, required, SelectInput, SimpleForm} from "react-admin";
import RichTextInput from "ra-input-rich-text";
import QuickCreateReferenceInput from '../../../Input/QuickCreate/QuickCreateReferenceInput'
import {dialogContent, selectArrayInput} from '../QuickCreate/Components'

export const LessonQuestionEdit = (props) => {
    return (
        <Edit {...props}>
            <SimpleForm>
                <ReferenceInput
                    source="lesson"
                    reference="lessons"
                    perPage={50}
                    allowEmpty={true}
                    sort={{ field: 'title', order: 'ASC' }}>
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <RichTextInput multiline source="question" validate={required()} />
                <RichTextInput source="wrongAnswerText" validate={required()} />
                <RichTextInput source="correctAnswerText" validate={required()} />
                <NumberInput source="position" step={1} min={0} validate={required()} />
                <QuickCreateReferenceInput
                    source="answers"
                    reference="lesson-question-answers"
                    label="Answers"
                    title="Create lesson question answer"
                    selectArrayInput={selectArrayInput}
                    dialogContent={dialogContent}
                    filter={{ "exists[lessonQuestion]": false }}
                />
            </SimpleForm>
        </Edit>
    );
}
