import React from "react";
import {
    Edit,
    SimpleForm,
    TextInput,
    required
} from 'react-admin';

export const FaqEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput multiline source="question" validate={required()} />
            <TextInput multiline source="answer" validate={required()} />
        </SimpleForm>
    </Edit>
);