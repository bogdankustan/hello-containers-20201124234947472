import React from "react";
import {
    Datagrid,
    EditButton,
    ReferenceField,
    ShowButton,
    TextField,
} from 'react-admin';
import ResourceIdField from "../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../Field/Common/CreatedAtField";
import UpdatedAtField from "../../../Field/Common/UpdatedAtField";
import LessonTestAttemptStatusField from "../../../Field/LessonTestAttempt/LessonTestAttemptStatusField";
import ListConfirmDelete from "../../../Element/List/ListConfirmDelete";

export const LessonTestAttemptsList = (props) => (
    <ListConfirmDelete {...props}>
        <Datagrid>
            <ResourceIdField record />
            <CreatedAtField record />
            <UpdatedAtField record />
            <ReferenceField label="Lesson" source="lesson" reference="lessons">
                <TextField source="title" />
            </ReferenceField>
            <ReferenceField label="User" source="user" reference="users">
                <TextField source="name" />
            </ReferenceField>
            <LessonTestAttemptStatusField component="text" addLabel />
            <ShowButton />
            <EditButton />
        </Datagrid>
    </ListConfirmDelete>
);