import React from "react";
import { Show } from 'react-admin';
import UserShowTabsLayout from "./ShowView/UserShowTabsLayout";

export const UserShow = (props) => (
    <Show {...props}>
        <UserShowTabsLayout />
    </Show>
);
