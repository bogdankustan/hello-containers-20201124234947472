import React from "react";
import { Tab, ReferenceArrayField, ReferenceField, Datagrid, TextField, ShowButton, } from 'react-admin';

const children = () => {
    return (
        <ReferenceArrayField
            reference="users"
            source="children">
            <Datagrid>
                <TextField source="name" />
                <TextField source="phone" />
                <ShowButton />
            </Datagrid>
        </ReferenceArrayField >
    );
}

const parent = () => {
    return (
        <ReferenceField source="parent" reference="users">
            <TextField source="name" />
        </ReferenceField>
    );
}

const AssociationsTab = (props) => {
    const { record: { isParent: isParent } } = props;
    return (
        <Tab label="Associations" path="associations" {...props}>
            {isParent ? children() : parent()}
        </Tab>
    )
};

export default AssociationsTab;
