import React from 'react';
import { keyBy } from 'lodash';
import { Datagrid, LinearProgress, Error, TextField } from 'react-admin';
import { Typography } from '@material-ui/core';
import { FieldProps } from "../../../../../../core/Types/common";
import { useFetchUserInvites } from "../../../../../../core/Hooks/Api";
import ResourceIdField from "../../../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../../../Field/Common/CreatedAtField";
import UserInviteStatusField from "../../../../../Field/User/UserInviteStatusField";
import PhoneField from "../../../../../Field/User/PhoneField";
import useStyles from "../../../../../../styles";
import UpdatedAtField from "../../../../../Field/Common/UpdatedAtField";

interface Props extends FieldProps {
    subject: 'sender' | 'receiver',
}

const subjectToName = { sender: 'receiverName', receiver: 'senderName' };
const subjectToPhone = { sender: 'receiverPhone', receiver: 'senderPhone' };
const subjectToLabel = { sender: 'To', receiver: 'From' };
const subjectToCaption = { sender: 'Sent invites', receiver: 'Received invites' };

const InvitesDataGrid = (props: Props) => {
    const classes = useStyles();
    const { subject, record: { id: user } } = props;
    const { data, error, loading } = useFetchUserInvites({
        filter: {
            [subject]: user
        }
    });

    if (!data) return null;
    if (loading) return <LinearProgress />;
    if (error) return <Error />;

    return (
        <>
            <Typography>{subjectToCaption[subject]}</Typography>
            <Datagrid
                data={keyBy(data, 'id')}
                ids={data.map(({ id }) => id)}
                currentSort={{ field: 'id', order: 'desc' }}
                className={classes.embedTable}
            >
                <ResourceIdField cellClassName={classes.narrow}/>
                <CreatedAtField cellClassName={classes.narrow}/>
                <UpdatedAtField cellClassName={classes.narrow}/>
                <TextField source={subjectToName[subject]} label={subjectToLabel[subject]} />
                <PhoneField source={subjectToPhone[subject]} cellClassName={classes.narrow}/>
                <UserInviteStatusField cellClassName={classes.narrow}/>
            </Datagrid>
        </>
    );
};

export default InvitesDataGrid;
