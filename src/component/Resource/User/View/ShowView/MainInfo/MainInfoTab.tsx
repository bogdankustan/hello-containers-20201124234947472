import React from "react";
import { ReferenceField, TextField, Tab, } from 'react-admin';
import UserTypeField from "../../../../../Field/User/UserTypeField";
import CreatedAtField from "../../../../../Field/Common/CreatedAtField";
import UpdatedAtField from "../../../../../Field/Common/UpdatedAtField";
import { FieldProps } from "../../../../../../core/Types/common";
import TimestampField from "../../../../../Field/Common/TimestampField";
import UserActiveStatusField from "../../../../../Field/User/UserActiveStatusField";

const MainInfoTab = (props: FieldProps) => (
    <Tab label="Main info" path="main" {...props}>
        <TextField source="name"/>
        <TextField source="phone"/>
        <UserTypeField component="text" addLabel/>
        {props.record && !props.record.isParent &&
        <ReferenceField source="lessonProgressLevel" reference="lesson-progress-levels">
            <TextField source="title" />
        </ReferenceField>
        }
        {props.record && !props.record.isParent && <TextField source="age" />}
        <UserActiveStatusField addLabel/>
        <CreatedAtField addLabel/>
        <TextField source="lastPhoneUpdateInfo" />
        <TimestampField addLabel source="lastPhoneUpdateDate" />
        <UpdatedAtField addLabel/>
    </Tab>
);

export default MainInfoTab;
