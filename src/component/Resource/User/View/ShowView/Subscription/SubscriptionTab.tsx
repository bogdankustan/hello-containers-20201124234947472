import React from 'react';
import { Datagrid, NumberField, ReferenceManyField, ReferenceField, Tab, TextField, FunctionField } from 'react-admin';
import TimestampField from "../../../../../Field/Common/TimestampField";
import useStyles from "../../../../../../styles";
import YesNoField from "../../../../../Field/Common/YesNoField";
import ResourceIdField from "../../../../../Field/Common/ResourceIdField";
import SubscriptionExpireStatusField from "../../../../../Field/UserSubscription/SubscriptionExpireStatusField";

const parentSubscriptions = (classes) => {
    return (
        <ReferenceManyField reference="user-subscriptions" source="childrenSubscriptions">
            <Datagrid>
                <ResourceIdField cellClassName={classes.narrow}/>
                <ReferenceField label="Subscriber" source="user" reference="users" cellClassName={classes.narrow}>
                    <TextField source="name"/>
                </ReferenceField>
                <ReferenceField label="Subscription" source="subscription" reference="subscriptions" cellClassName={classes.fw200}>
                    <TextField source="title"/>
                </ReferenceField>
                <NumberField label="Price" source="finalPrice" cellClassName={classes.narrow}/>
                <ReferenceField label="Discount code" source="discountCode" reference="discount-codes" cellClassName={classes.fw125}>
                    <TextField source="title"/>
                </ReferenceField>
                <FunctionField label="Paid" render={item => <YesNoField record={item} source="paid"/>} cellClassName={classes.narrow}/>
                <TimestampField source="activeSince" cellClassName={classes.narrow}/>
                <TimestampField source="endDate" cellClassName={classes.narrow}/>
                <FunctionField label="Status" render={item => <SubscriptionExpireStatusField record={item}/>} cellClassName={classes.narrow}/>
            </Datagrid>
        </ReferenceManyField>
    )
}

const childrenSubscriptions = (classes) => {
    return (
        <ReferenceManyField reference="user-subscriptions" source="subscriptions">
            <Datagrid>
                <ResourceIdField cellClassName={classes.narrow}/>
                <ReferenceField label="Subscription" source="subscription" reference="subscriptions" cellClassName={classes.fw200}>
                    <TextField source="title"/>
                </ReferenceField>
                <NumberField label="Price" source="finalPrice" cellClassName={classes.narrow}/>
                <ReferenceField label="Discount code" source="discountCode" reference="discount-codes" cellClassName={classes.fw125}>
                    <TextField source="title"/>
                </ReferenceField>
                <FunctionField label="Paid" render={item => <YesNoField record={item} source="paid"/>} cellClassName={classes.narrow}/>
                <TimestampField source="activeSince" cellClassName={classes.narrow}/>
                <TimestampField source="endDate" cellClassName={classes.narrow}/>
                <FunctionField label="Status" render={item => <SubscriptionExpireStatusField record={item}/>} cellClassName={classes.narrow}/>
            </Datagrid>
        </ReferenceManyField>
    )
}

const SubscriptionTab = (props) => {
    const { record: { isParent: isParent } } = props;
    const classes = useStyles();
    return (
        <Tab label="Subscription" path="user-subscription" {...props}>
            {isParent ? parentSubscriptions(classes) : childrenSubscriptions(classes)}
        </Tab>
    )
};

export default SubscriptionTab;
