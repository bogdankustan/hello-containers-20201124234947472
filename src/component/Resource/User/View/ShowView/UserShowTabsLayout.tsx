import React from "react";
import { TabbedShowLayout } from 'react-admin';
import MainInfoTab from "./MainInfo/MainInfoTab";
import AssociationsTab from "./Associations/AssociationsTab";
import WalletsTab from "./Wallets/WalletsTab";
import GoalsTab from "./Goals/GoalsTab";
import DepositsTab from "./Deposits/DepositsTab";
import TasksTab from "./Tasks/TasksTab";
import TransactionsTab from "./Transactions/TransactionsTab";
import SubscriptionTab from "./Subscription/SubscriptionTab";

const UserShowTabsLayout = (props) => (
    <TabbedShowLayout {...props}>
        <MainInfoTab />
        <AssociationsTab record={props.record} />
        <WalletsTab record={props.record} />
        <GoalsTab />
        <DepositsTab />
        <TasksTab record={props.record} />
        <TransactionsTab />
        <SubscriptionTab record={props.record}/>
    </TabbedShowLayout>
)

export default UserShowTabsLayout;
