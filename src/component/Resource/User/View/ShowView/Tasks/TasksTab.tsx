import React from "react";
import {
    Datagrid,
    NumberField,
    ReferenceField,
    ReferenceManyField,
    ShowButton,
    Tab,
    TextField
} from 'react-admin';
import TaskStatusField from "../../../../../Field/Task/TaskStatusField";


const taskList = (isParent: boolean) => {
    if (isParent) {
        return (
            <ReferenceManyField
                reference="tasks"
                target="owner"
                label=""
            >
                <Datagrid>
                    <ReferenceField source="assignee" reference="users">
                        <TextField source="name" />
                    </ReferenceField>
                    <ReferenceField source="recommendedTask" reference="recommended-tasks">
                        <TextField source="title" />
                    </ReferenceField>
                    <TextField source="title" />
                    <NumberField source="estimatedPay" />
                    <TaskStatusField component="text" addLabel />
                    <ShowButton />
                </Datagrid>
            </ReferenceManyField >
        );
    }

    return (
        <ReferenceManyField
            reference="tasks"
            target="assignee"
            label=""
        >
            <Datagrid>
                <ReferenceField source="owner" reference="users">
                    <TextField source="name" />
                </ReferenceField>
                <ReferenceField source="recommendedTask" reference="recommended-tasks">
                    <TextField source="title" />
                </ReferenceField>
                <TextField source="title" />
                <NumberField source="estimatedPay" />
                <TaskStatusField component="text" addLabel />
                <ShowButton />
            </Datagrid>
        </ReferenceManyField >
    );
}

const TasksTab = (props) => {
    const { record: { isParent: isParent } } = props;
    return (
        <Tab label="Tasks" path="tasks" {...props}>
            { taskList(isParent) }
        </Tab>
    )
}

export default TasksTab;
