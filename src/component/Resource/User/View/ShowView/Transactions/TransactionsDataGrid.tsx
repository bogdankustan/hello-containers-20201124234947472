import React from 'react';
import { keyBy } from 'lodash';
import { Datagrid, LinearProgress, Error, TextField } from 'react-admin';
import { useFetchUserRelatedTransactions } from "../../../../../../core/Hooks/Api";
import useStyles from "../../../../../../styles";
import ResourceIdField from "../../../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../../../Field/Common/CreatedAtField";
import PhoneField from "../../../../../Field/User/PhoneField";
import TransactionStatusField from "../../../../../Field/Wallet/TransactionStatusField";
import WalletTransactionTypeField from "../../../../../Field/Wallet/WalletTransactionTypeField";
import MoneyAmountField from "../../../../../Field/Wallet/MoneyAmountField";

const PersonNameField = ({subject, ...props}) => (
    <TextField
        {...props}
        source={props.record.sender === subject ? 'beneficiaryName' : 'senderName'}
    />
)

const PersonPhoneField = ({subject,...props}) => (
    <PhoneField
        {...props}
        source={props.record.sender === subject ? 'beneficiaryPhone' : 'senderPhone'}
    />
)

//TODO: implement pagination
const TransactionsDataGrid = (props) => {
    const classes = useStyles();
    const { record: { id: user } } = props;

    const { loading, error, data } = useFetchUserRelatedTransactions({
        id: user,
        page: 0
    });

    if (loading) return <LinearProgress />;
    if (error) return <Error />;
    if (!data) return null;

    return (
        <>
            <Datagrid
                data={keyBy(data, 'id')}
                ids={data.map(({ id }) => id)}
                currentSort={{ field: 'id', order: 'desc' }}
            >
                <ResourceIdField cellClassName={classes.narrow}/>
                <CreatedAtField cellClassName={classes.narrow}/>
                <MoneyAmountField cellClassName={classes.narrow}/>
                <PersonNameField cellClassName={classes.narrow} subject={user} label="Name"/>
                <PersonPhoneField cellClassName={classes.narrow} subject={user} label="Phone"/>
                <TextField source="comment" />
                <WalletTransactionTypeField cellClassName={classes.narrow}/>
                <TransactionStatusField cellClassName={classes.narrow}/>
            </Datagrid>
        </>
    );
}

export default TransactionsDataGrid;
