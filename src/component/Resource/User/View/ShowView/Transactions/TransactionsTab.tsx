import React from "react";
import { Datagrid, ReferenceManyField, ShowButton, Tab, TextField } from 'react-admin';
import TransactionStatusField from "../../../../../Field/Wallet/TransactionStatusField";
import ResourceIdField from "../../../../../Field/Common/ResourceIdField";
import CreatedAtField from "../../../../../Field/Common/CreatedAtField";
import MoneyAmountField from "../../../../../Field/Wallet/MoneyAmountField";

const TransactionsTab = (props) => {
    return (
    <Tab label="Transactions" path="transactions" {...props}>
        <ReferenceManyField source="wallet" reference="wallet-transactions" target="senderWallet" label="Outgoing transactions">
            <Datagrid>
                <ResourceIdField/>
                <CreatedAtField/>
                <TextField source="beneficiaryName"/>
                <MoneyAmountField/>
                <TransactionStatusField addLabel component="text"/>
                <TextField source="comment"/>
                <ShowButton/>
            </Datagrid>
        </ReferenceManyField>

        <ReferenceManyField source="wallet" reference="wallet-transactions" target="beneficiaryWallet" label="Incoming transactions">
            <Datagrid>
                <ResourceIdField/>
                <CreatedAtField/>
                <TextField source="senderName"/>
                <MoneyAmountField/>
                <TransactionStatusField addLabel component="text"/>
                <TextField source="comment"/>
                <ShowButton/>
            </Datagrid>
        </ReferenceManyField>
    </Tab>
    )
}

export default TransactionsTab;
