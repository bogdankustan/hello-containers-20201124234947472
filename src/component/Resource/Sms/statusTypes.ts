export const StatusChoices = [
    { id: 1, name: 'Confirm code' },
    { id: 2, name: 'Invite for relation' },
    { id: 3, name: 'Invite new user' },
];