import React from "react";
import {Edit, SimpleForm} from 'react-admin';

export default (props) => {
    return (
        <Edit {...props}>
            <SimpleForm>
            </SimpleForm>
        </Edit>
    );
}
