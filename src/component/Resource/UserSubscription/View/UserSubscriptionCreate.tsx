import React from "react";
import {
    Create,
    DateTimeInput,
    NullableBooleanInput,
    ReferenceInput,
    SelectInput,
    SimpleForm,
    required
} from 'react-admin';

export default (props) => {
    return (
        <Create {...props}>
            <SimpleForm redirect='/user-subscriptions'>
                <ReferenceInput
                    source="user"
                    reference="users"
                    perPage={50}
                    sort={{ field: 'name', order: 'ASC' }}
                    validate={required()}>
                    <SelectInput optionText="name" />
                </ReferenceInput>
                <ReferenceInput
                    source="subscription"
                    reference="subscriptions"
                    perPage={50}
                    sort={{ field: 'title', order: 'ASC' }}
                    validate={required()}>
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <ReferenceInput
                    source="discountCode"
                    reference="discount-codes"
                    perPage={50}
                    sort={{ field: 'title', order: 'ASC' }}>
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <NullableBooleanInput source="paid" validate={required()} />
                <DateTimeInput source="activeSince" />
            </SimpleForm>
        </Create>
    );
}
