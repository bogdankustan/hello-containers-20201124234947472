import {SessionUser} from "./SessionUser";

const storageKey = 'sessionUser';

export const userStorage = {
    setUser: (data: string) => {
        sessionStorage.setItem(storageKey, JSON.stringify(data));
    },
    getUser: (): SessionUser|null => {
        const data = sessionStorage.getItem(storageKey);
        return data ? SessionUser.fromArray(JSON.parse(data)) : null;
    },
    hasUser: (): boolean => {
        return !!sessionStorage.getItem(storageKey);
    },
    removeUser: (): void => {
        sessionStorage.removeItem(storageKey);
    }
}