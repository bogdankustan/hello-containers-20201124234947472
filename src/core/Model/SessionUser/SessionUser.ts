import { ROLE_ADMIN, ROLE_SUPER_ADMIN } from "../../Types/roles";

export class SessionUser {
    name: string;
    roles: string[];
    expires: number = 0;

    constructor(name: string, roles: string[], expires = 0) {
        this.name = name;
        this.roles = roles;
        this.expires = expires;
    }

    hasRole(role: string): boolean {
        return this.roles.indexOf(role) !== -1;
    }

    isAdmin(): boolean {
        return this.hasRole(ROLE_ADMIN);
    }

    isSuperAdmin(): boolean {
        return this.hasRole(ROLE_SUPER_ADMIN);
    }

    static fromArray({ name, roles, expires, exp }): SessionUser {
        return new SessionUser(name, roles, expires ? expires : exp);
    }
}
