import { merge, trim } from 'lodash';
import { useQueryWithStore } from 'react-admin';
import config from "../../config";
import { buildApiHeaders, formatApiUrl } from "../../auth/utils";
const axios = require('axios');

interface QueryResponseInterface {
    data: undefined|any|any[],
    total: null|number,
    error: null|object|string,
    loading: boolean
    loaded: boolean
}

export const useFetchUserInvites = (payload): QueryResponseInterface => (
    useQueryWithStore(
        merge(
            {
                type: 'getList',
                resource: 'user-parent-invites',
                payload: {
                    pagination: {
                        page: 0,
                        perPage: config.api.pagination.itemsPerPage
                    },
                    sort: {
                        field: 'id',
                        order: 'desc'
                    },
                }
            },
            { payload }
        )
    )
);

export const useFetchRelatedUserInvites = ({ id }) => (
    useFetchUserInvites({
        filter: {
            relatedUser: id
        }
    })
);

export const useFetchSentInvitations = ({ user })  => (
    useFetchUserInvites({
        filter: {
            sender: user
        }
    })
);

export const useFetchReceivedInvitations = ({ user })  => (
    useFetchUserInvites({
        filter: {
            receiver: user
        }
    })
);

export const useFetchUserTransactions = (payload, page = 0): QueryResponseInterface => (
    useQueryWithStore(
        merge(
            {
                type: 'getList',
                resource: 'wallet-transactions',
                payload: {
                    pagination: {
                        page,
                        perPage: config.api.pagination.itemsPerPage
                    },
                    sort: {
                        field: 'id',
                        order: 'desc'
                    },
                }
            },
            { payload }
        )
    )
);

export const useFetchUserRelatedTransactions = ({ id, page }): QueryResponseInterface => (
    useFetchUserTransactions({
        filter: {
            relatedUser: id
        }
    }, page)
);

export const getResourcePagination = (resource) => {
    const parsePage = (s: string): number => {
        const match = /page=(\d+)/.exec(s);
        return match !== null ? parseInt(match[1]) : 1;
    }
    // // fetch data from a url endpoint
    // const data = await axios.get("/some_url_endpoint")
    //     .then((result) => return result.names)
    //
    // return data;
    return axios
        .get(formatApiUrl(trim(resource, '/')), { headers: buildApiHeaders() })
        .then(({ data }) => {
            console.log(data);
            const hydraView = !!data['hydra:view'] ? data['hydra:view'] : null;
            const currentPageUrl = hydraView ? hydraView['@id'] : data['@id'];
            const nextPageUrl = hydraView ? hydraView['hydra:next'] : currentPageUrl;
            const firstPageUrl = hydraView ? hydraView['hydra:first'] : currentPageUrl;
            const lastPageUrl = hydraView ? hydraView['hydra:last'] : currentPageUrl;
            const totalItems = data['hydra:totalItems'];

            return {
                currentPageUrl,
                nextPageUrl,
                firstPageUrl,
                lastPageUrl,
                currentPage: parsePage(currentPageUrl),
                nextPage: parsePage(nextPageUrl),
                firstPage: parsePage(firstPageUrl),
                lastPage: parsePage(lastPageUrl),
                totalItems,
            }
        })
        .catch(error => error)
    ;

    //return {result};

    // try {
    //     const result = axios
    //         .get(formatApiUrl(resource), { headers: buildApiHeaders() })
    //         .then(({ data }) => data)
    //         .catch(error => error)
    //     ;
    //
    //     return result;
    // } catch (error) {
    //     if (config.debug) {
    //         console.log(error)
    //     }
    //
    //     return 1;
    // }

    /*
    return await axios
        .get(
            formatApiUrl(resource),
            { headers: buildApiHeaders() }
        )
        .then(function ({ data }) {
            const currentPageUrl = data['hydra:view']['@id'];
            const nextPageUrl = data['hydra:view']['hydra:next'];
            const firstPageUrl = data['hydra:view']['hydra:first'];
            const lastPageUrl = data['hydra:view']['hydra:last'];
            const totalItems = data['hydra:totalItems'];

            return {
                currentPageUrl,
                nextPageUrl,
                firstPageUrl,
                lastPageUrl,
                currentPage: parsePage(currentPageUrl),
                nextPag: parsePage(nextPageUrl),
                firstPage: parsePage(firstPageUrl),
                lastPage: parsePage(lastPageUrl),
                totalItems,
            }
        })
        .catch(function (error) {
            return Promise.reject(error);
        })
    */
}