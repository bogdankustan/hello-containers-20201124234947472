export type NullableString = string | null;

export interface FieldProps {
    source?: any,
    record?: any,
    [key: string]: any
}
