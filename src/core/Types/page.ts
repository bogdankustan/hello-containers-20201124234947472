export const CATEGORY_PRIVACY_POLICY = 'Privacy policy';
export const CATEGORY_TERMS_AND_CONDITIONS = 'Terms and conditions';
export const CATEGORY_SECURITY = 'Security';
export const CATEGORY_LESSON_PROPOSAL = 'Lesson proposal';
export const CATEGORY_OTHER = 'Other';
