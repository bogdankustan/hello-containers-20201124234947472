interface ConfigInterface {
    locale: string,
    dateTimeLocale: string,
    debug: boolean,
    translationLanguages: string[],
    money: {
        currency: string,
        locale: string,
        precision: number,
        factor: number
    },
    api: {
        entrypoint: {
            root: string,
            adminLogin: string,
            adminLogout: string
        },
        token: {
            headerName: string,
            valuePrefix: string,
            serverTokenKey: string,
            localTokenKey: string
        },
        pagination: {
            itemsPerPage: number
        }
    }
}

export default ConfigInterface;
