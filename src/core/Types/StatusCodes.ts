export const STATUS_REJECTED = -10;
export const STATUS_APPROVING = 1;
export const STATUS_IN_PROGRESS = 10;
export const STATUS_EVALUATING = 20;
export const STATUS_COMPLETED = 30;
export const STATUS_PENDING = 40;
export const STATUS_APPROVED = 50;
