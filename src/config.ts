import ConfigInterface from "./core/Types/ConfigType";

const config: ConfigInterface = {
    locale: 'en',
    dateTimeLocale: 'lt-LT',
    translationLanguages: ['lt'],
    debug: true,
    money: {
        currency: 'EUR',
        locale: 'lt-LT',
        precision: 2,
        factor: 0.01
    },
    api: {
        entrypoint: {
            root: `${process.env.REACT_APP_API_ENTRYPOINT}`,
            adminLogin: `${process.env.REACT_APP_ADMIN_ENTRYPOINT}/login`,
            adminLogout: `${process.env.REACT_APP_ADMIN_ENTRYPOINT}/logout`
        },
        token: {
            headerName: 'Authorization',
            valuePrefix: 'Bearer ',
            serverTokenKey: 'token',
            localTokenKey: 'token',
        },
        pagination: {
            itemsPerPage: 25
        }
    }
};

export const appWindows = [
    { id: 'WalletTab',                  name: 'WalletTab'               },
    { id: 'TasksTab',                   name: 'TasksTab'                },
    { id: 'LessonsTab',                 name: 'LessonsTab'              },
    { id: 'SettingsTab',                name: 'SettingsTab'             },
    { id: 'CardTab',                    name: 'CardTab'                 },
    { id: 'TopicView_parent',           name: 'TopicView_parent'        },
    { id: 'lessonTipsListContainer',    name: 'lessonTipsListContainer' }
];

export default config;
